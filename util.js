define([], function() {

    let Utils = {

        xhr_get: function(url) {

            return new Promise((resolve, reject) => {

                var xhr = new XMLHttpRequest();
                xhr.open('GET', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.onreadystatechange = function() {
                    if (this.readyState == 4) {
                        if (this.status == 200) {

                            let data = xhr.responseText;

                            let content_type = xhr.getResponseHeader('Content-Type');

                            if (content_type == 'image/svg+xml') {
                                data = xhr.responseXML.documentElement;
                            }

                            if (content_type == 'application/json') {
                                try {
                                    data = JSON.parse(data);
                                } catch (error) {
                                    data = null;
                                }
                            }

                            resolve(data);
                        } else {
                            reject(xhr);
                        }
                    }
                };
                xhr.send();

            });

        },

    };

    return Utils;

});
