define(['util'], function(util) {

    function MapMN(opts) {

        this.el = opts.el;
        this.cb_hover = opts.onhover;
        this.cb_click = opts.onclick;
        this.cb_load = opts.onload;

        this.color_sum = opts.colors.sum;
        this.color_aimag = opts.colors.aimag;

        this.sum_list = {};
        this.sum_current = null;

        this.init();

    }

    MapMN.prototype.init = function() {

        let url_svg = this.el.getAttribute('data-svg');
        let url_json = this.el.getAttribute('data-json');

        let promise_svg = util.xhr_get(url_svg);
        let promise_json = util.xhr_get(url_json);

        Promise.all([promise_svg, promise_json]).then(values => {
            let [svg, data] = values;

            this.el.appendChild(svg);

            for (let aimag of data) {

                let dom_aimag = svg.getElementById(aimag.id);
                [dom_aimag.style.fill, dom_aimag.style.stroke] = this.color_aimag.normal;
                dom_aimag.style.strokeWidth = 3;

                for (let sum of aimag.sum_list) {

                    let dom_sum = svg.getElementById(sum.id);
                    [dom_sum.style.fill, dom_sum.style.stroke] = this.color_sum.normal;

                    dom_sum.addEventListener('mouseenter', (e) => {
                        this.highlight(sum.id);
                        this.onmouseenter(sum.id);
                    });
                    dom_sum.addEventListener('mouseleave', (e) => {
                        this.onmouseleave(sum.id);
                    });
                    dom_sum.addEventListener('click', (e) => {
                        this.onclick(sum.id);
                    });

                    this.sum_list[sum.id] = {
                        el: dom_sum,
                        aimag: {
                            el: dom_aimag,
                        },
                    }

                }
            }

            this.onload(data);

        });

    }

    MapMN.prototype.onload = function(data) {
        if (!this.cb_load) return;
        this.cb_load(data);
    }

    MapMN.prototype.onclick = function(id) {
        if (!this.cb_click) return;
        this.cb_click(id);
    }

    MapMN.prototype.onmouseenter = function(id) {
        if (!this.cb_hover) return;
        this.highlight(id);
        this.cb_hover(id);
    }

    MapMN.prototype.onmouseleave = function(aimag, sum) {
        this.blur();
    }

    MapMN.prototype.blur = function() {
        if (!this.sum_current) return;

        let dom_sum = this.sum_current.el;
        let dom_aimag = this.sum_current.aimag.el;

        [dom_sum.style.fill, dom_sum.style.stroke] = this.color_sum.normal;
        [dom_aimag.style.fill, dom_aimag.style.stroke] = this.color_aimag.normal;

        this.sum_current = null;
    }

    MapMN.prototype.highlight = function(id) {

        this.blur();

        if (!(id in this.sum_list)) return;

        let sum = this.sum_list[id];

        let dom_sum = sum.el;
        let dom_aimag = sum.aimag.el;

        [dom_sum.style.fill, dom_sum.style.stroke] = this.color_sum.highlight;
        [dom_aimag.style.fill, dom_aimag.style.stroke] = this.color_aimag.highlight;

        this.sum_current = sum;
    }

    return MapMN;

});
