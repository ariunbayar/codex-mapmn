requirejs(['MapMN',], function(MapMN){

    let container_sum = document.querySelector('#select_sum');

    let map = new MapMN({

        el: document.querySelector('.mapMN'),

        colors: {
            sum: {
                normal: ['none', '#fff'],
                highlight: ['#F8BBD0', '#fff']
            },
            aimag: {
                normal: ['#eee', '#05458f'],
                highlight: ['#ddd', '#05458f']
            }
        },

        onhover: function(id) {
        },

        onclick: function(id) {
            container_sum.value = id;
        },

        onload: function(data) {
            for (let aimag of data) {
                for (let sum of aimag.sum_list) {
                    let option_sum = document.createElement('option');
                    option_sum.setAttribute('value', sum.id);
                    option_sum.appendChild(document.createTextNode(aimag.name + ' - ' + sum.name));
                    container_sum.appendChild(option_sum);
                }
            }

            container_sum.addEventListener('change', (e) => {
                this.highlight(e.target.value);
            });
            container_sum.value = data[0].sum_list[0].id;
            container_sum.dispatchEvent(new Event('change'));
        }
    });

})();
